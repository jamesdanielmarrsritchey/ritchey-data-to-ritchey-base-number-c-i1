<?php
#Name:Data To Ritchey Base Number C v1
#Description:Convert a data to a number using the Ritchey Base Number C encoding scheme. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'source' (required) is a path to the file to convert. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('data_to_ritchey_base_number_c_v1') === FALSE){
function data_to_ritchey_base_number_c_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@empty($string) === TRUE){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert string to Base64. Break Base64 string into an array of single characters. Represent each letter as an incrementing even number between 2222 and 4222, excluding any numbers containing zero (eg: A = 2222, B = 2224, C = 2226). Convert array to string. NOTE: Base64 can encode be done in chunks of 3 bytes and still achieve the same result as processing a file all at once, but this implementation does not process files 3 bytes at a time. There is no point since it is returning the number, not saving it to another file.]
	if (@empty($errors) === TRUE){
		###Convert to base64
		$string = @base64_encode($string);
		###Convert string to array of single characters
		$string = @str_split($string, 1);
		###Convert each character in the array to a number between 2222 and 8248.
		foreach ($string as &$character){
			if ($character === "A"){
				$character = '2222';
			} else if ($character === "B"){
				$character = '2224';
			} else if ($character === "C"){
				$character = '2226';
			} else if ($character === "D"){
				$character = '2228';
			} else if ($character === "E"){
				$character = '2242';
			} else if ($character === "F"){
				$character = '2244';
			} else if ($character === "G"){
				$character = '2246';
			} else if ($character === "H"){
				$character = '2248';
			} else if ($character === "I"){
				$character = '2262';	
			} else if ($character === "J"){
				$character = '2264';	
			} else if ($character === "K"){
				$character = '2266';	
			} else if ($character === "L"){
				$character = '2268';	
			} else if ($character === "M"){
				$character = '2282';	
			} else if ($character === "N"){
				$character = '2284';	
			} else if ($character === "O"){
				$character = '2286';	
			} else if ($character === "P"){
				$character = '2288';	
			} else if ($character === "Q"){
				$character = '2422';	
			} else if ($character === "R"){
				$character = '2424';	
			} else if ($character === "S"){
				$character = '2426';	
			} else if ($character === "T"){
				$character = '2428';	
			} else if ($character === "U"){
				$character = '2442';	
			} else if ($character === "V"){
				$character = '2444';	
			} else if ($character === "W"){
				$character = '2446';	
			} else if ($character === "X"){
				$character = '2448';	
			} else if ($character === "Y"){
				$character = '2462';	
			} else if ($character === "Z"){
				$character = '2464';	
			} else if ($character === "a"){
				$character = '2466';	
			} else if ($character === "b"){
				$character = '2468';
			} else if ($character === "c"){
				$character = '2482';
			} else if ($character === "d"){
				$character = '2484';
			} else if ($character === "e"){
				$character = '2486';
			} else if ($character === "f"){
				$character = '2488';
			} else if ($character === "g"){
				$character = '2622';
			} else if ($character === "h"){
				$character = '2624';
			} else if ($character === "i"){
				$character = '2626';	
			} else if ($character === "j"){
				$character = '2628';	
			} else if ($character === "k"){
				$character = '2642';	
			} else if ($character === "l"){
				$character = '2644';	
			} else if ($character === "m"){
				$character = '2646';	
			} else if ($character === "n"){
				$character = '2648';	
			} else if ($character === "o"){
				$character = '2662';	
			} else if ($character === "p"){
				$character = '2664';	
			} else if ($character === "q"){
				$character = '2666';	
			} else if ($character === "r"){
				$character = '2668';	
			} else if ($character === "s"){
				$character = '2682';	
			} else if ($character === "t"){
				$character = '2684';	
			} else if ($character === "u"){
				$character = '2686';	
			} else if ($character === "v"){
				$character = '2688';	
			} else if ($character === "w"){
				$character = '2822';	
			} else if ($character === "x"){
				$character = '2824';	
			} else if ($character === "y"){
				$character = '2826';	
			} else if ($character === "z"){
				$character = '2828';	
			} else if ($character === "0"){
				$character = '2842';	
			} else if ($character === "1"){
				$character = '2844';	
			} else if ($character === "2"){
				$character = '2846';	
			} else if ($character === "3"){
				$character = '2848';	
			} else if ($character === "4"){
				$character = '2862';	
			} else if ($character === "5"){
				$character = '2864';	
			} else if ($character === "6"){
				$character = '2866';	
			} else if ($character === "7"){
				$character = '2868';	
			} else if ($character === "8"){
				$character = '2882';	
			} else if ($character === "9"){
				$character = '2884';	
			} else if ($character === "+"){
				$character = '2886';	
			} else if ($character === "/"){
				$character = '2888';	
			} else if ($character === "="){
				$character = '4222';	
			} else {
				$errors[] = "task - Convert characters to numbers";
				goto result;
			}
		}
		###Convert array back to string.
		$string = @implode($string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('data_to_ritchey_base_number_c_v1_format_error') === FALSE){
			function data_to_ritchey_base_number_c_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("data_to_ritchey_base_number_c_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>